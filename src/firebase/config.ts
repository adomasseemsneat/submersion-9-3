


import firebase from 'firebase/app'
import 'firebase/firestore'

const firebaseConfig = {
  apiKey: "AIzaSyBqqTd54wbCpK4IKDRcGCD3wSmSKcW3HzY",
  authDomain: "submersion-9.firebaseapp.com",
  projectId: "submersion-9",
  storageBucket: "submersion-9.appspot.com",
  messagingSenderId: "443319495869",
  appId: "1:443319495869:web:ef1c5d2f9b44ba0c12dbd4",
  measurementId: "G-2M43MNLPQN"
};  



firebase.initializeApp(firebaseConfig)

const fire = firebase.firestore()
// const auth = firebase.auth()

export {fire}